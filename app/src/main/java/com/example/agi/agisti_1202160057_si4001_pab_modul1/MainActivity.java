package com.example.agi.agisti_1202160057_si4001_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button buttonCek;
    TextView inputAlas, inputTinggi, liatHasil;
    int jawaban;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputAlas = (TextView)findViewById(R.id.iniAlas);
        inputTinggi = (TextView)findViewById(R.id.iniTinggi);
        buttonCek = (Button)findViewById(R.id.buttonCek);
        liatHasil = (TextView)findViewById(R.id.textHasil);

        buttonCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputAlas.length()==0 && inputTinggi.length()==0){
                    Toast.makeText(getApplication(),"alas dan tinggi tidak boleh kosong", Toast.LENGTH_LONG).show();
                }else if(inputTinggi.length()==0){
                    Toast.makeText(getApplication(),"tinggi tidak boleh kosong", Toast.LENGTH_LONG).show();
                }else if(inputAlas.length()==0){
                    Toast.makeText(getApplication(),"alas tidak boleh kosong", Toast.LENGTH_LONG).show();
                }else{
                    String alas = inputAlas.getText().toString();
                    String tinggi = inputTinggi.getText().toString();

                    double alasalas = Double.parseDouble(alas);
                    double tinggitinggi = Double.parseDouble(tinggi);

                    double hasilnya = (alasalas*tinggitinggi);

                    String output = String.valueOf(hasilnya);
                    liatHasil.setText(output.toString());


                }
            }
        });
    }
}
